#ifndef BIOTOP_H
#define BIOTOP_H
#include <QtCore>
#include <QtWidgets>
#include "biest.h"
#include "standardparameter.h"

//-------------------------Biotop--------------------------------------
class Oase{
 public:
    Oase():bereich(){}
    Oase(const QRect& hier,int rate);//bei rate<0 wird nach der Mannarate gefragt.
    QRect bereich;
    void setRate(int rate);
    int getRate()const{return relativerate;}
    int getAbsoluterate()const{return absoluterate;}
 private:
    int relativerate=0;
    int absoluterate=0;//zusätzlicher Regen für die Oase
    friend QDataStream&operator<<(QDataStream&ds,const Oase&oas);
    friend QDataStream&operator>>(QDataStream&ds,Oase&oas);
};

class Biotop : public QWidget
{ 
    Q_OBJECT
public:
    Biotop( QWidget* parent = nullptr,QSize siz=QSize(900,700));
    ~Biotop();
    void showPopulation();
    int bakterienZahl();
    //bool oaseBearbeiten(Oase*oase);//true, wenn Oase verändert wurde
    //uint abbiegrichtung=1;
    //void biestBearbeiten(QPoint pos){(*biestvolk)[biestvolk->biestAt(pos)].design();};
public slots:
    virtual void paintEvent( QPaintEvent * );
    virtual void mousePressEvent(QMouseEvent*e);
    void rechtsKlick(QMouseEvent*e);
    virtual void mouseReleaseEvent(QMouseEvent * e);
    virtual void mouseMoveEvent( QMouseEvent * e );
    virtual void resizeEvent(QResizeEvent * e);
    //void changeDelay();
    void changeDelay(int newDelay);
    //void showOasen(bool show);
    void pause(){if (timer->isActive()) timer->stop(); else timer->start(delay);}
    void changeMannarate(int newMannarate);
    void regnetManna(int soviel=-1);
    void nextStep();//wird vom Timer aufgerufen.
    //void changeSettings();
    void restart();//wird aufgerufen, wenn alles tot ist, sofern gewünscht.
    void renew(bool andRestart=true);//erlaubt die Änderung von Parametern.
    void startStop(ushort state=2);//0=stop, 1=start, 2=toggle
public:
    QTimer * timer;//sorgt für regelmäßigen update
    int delay;        //Verzögerung in ms zwischen zwei Zyklen.
    BiestPopulation * biestvolk;
    QImage * bakterien;
    uint lifetime=0; 
    int Mannarate=Standard::Mannarate;//soviel Bakterien pro 1000.000 pixel pro Schritt.
    int Nahrhaftigkeit=Standard::Nahrhaftigkeit;//soviel Schritte kann ein Biest mit einem Bakterium gehen.
    int Startpopulation=Standard::Startpopulation;
private:
    void forceParentResize(const QSize &size);///zwingt das ParentWidget (MainWindow) die Größe so zu verändern, dass this die Größe size bekommt.
    QList<Oase> oasen;
    //bool oasenZeichnen;
    uint absoluterate=Mannarate;//Mannarate*size()/1 Mio.
    uint abbiegrichtung=1;
    bool selecting=false;//zeigt an, dass gerade ein Feld selektiert wird
    QPoint startSelect;//zeigt an, wo das Selektieren begonnen hat
    QRect selectedRect;//zeigt das selektierte Gebiet an.
    QLabel * sizeLabel;
    friend QDataStream&operator<<(QDataStream&ds,const Biotop&bio);
    friend QDataStream&operator>>(QDataStream&ds,Biotop&bio);

signals:
   void mannarateChanged(uint rate);
};

#endif
