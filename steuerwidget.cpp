#include "steuerwidget.h"
#include "ui_steuerwidget.h"
#include "biotop.h"
#include "biotopmainwindow.h"
Steuerwidget::Steuerwidget(QWidget *parent,Biotop*biot) :
    QDockWidget(parent),
    ui(new Ui::Steuerwidget)
{
    biotop=biot;
    ui->setupUi(this);
    ui->delay->setValue(biotop->delay);
    ui->mannarate->setValue(biot->Mannarate);
    ui->mannaLabel->setText(QString::number(biot->Mannarate));
    connect(ui->mannarate,&QSlider::valueChanged,[this](int val){biotop->changeMannarate(val);ui->mannaLabel->setText(QString::number(val));});
    connect(ui->delay,&QSpinBox::valueChanged,[this](int val){biotop->changeDelay(val);});
    connect(ui->pauseButton,&QPushButton::clicked,[this](){biotop->startStop();});
    connect(ui->synchronize,&QCheckBox::stateChanged,BiotopMainWindow::synchronizer,&BiotopSynchronizer::setActive);
    connect(ui->backgroundSlider,&QSlider::valueChanged,[this](int val){biotop->bakterien->setColor(0,QColor(val,val,val).rgb());});
    connect(biotop,&Biotop::mannarateChanged,ui->mannarate,&QSlider::setValue);
    actualize();
}
Steuerwidget::~Steuerwidget()
{
    delete ui;
}

void Steuerwidget::actualize()
{
   ui->lifetime->setText(QString::number(biotop->lifetime));
   ui->biestcount->setText(QString::number(biotop->biestvolk->living));
   Biest dbiest=biotop->biestvolk->durchschnittsBiest();
   const short prec=3;
   ui->vl->setText(QString::number(dbiest.dirPrefs[vl+8],'f',prec));
   ui->vo->setText(QString::number(dbiest.dirPrefs[vo+8],'f',prec));
   ui->vr->setText(QString::number(dbiest.dirPrefs[vr+8],'f',prec));
   ui->li->setText(QString::number(dbiest.dirPrefs[li+8],'f',prec));
   ui->re->setText(QString::number(dbiest.dirPrefs[re+8],'f',prec));
   ui->hl->setText(QString::number(dbiest.dirPrefs[hl+8],'f',prec));
   ui->hi->setText(QString::number(dbiest.dirPrefs[hi+8],'f',prec));
   ui->hr->setText(QString::number(dbiest.dirPrefs[hr+8],'f',prec));
   //---------------------
   ui->vl_2->setText(QString::number(dbiest.dirPrefs[vl],'f',prec));
   ui->vo_2->setText(QString::number(dbiest.dirPrefs[vo],'f',prec));
   ui->vr_2->setText(QString::number(dbiest.dirPrefs[vr],'f',prec));
   ui->li_2->setText(QString::number(dbiest.dirPrefs[li],'f',prec));
   ui->re_2->setText(QString::number(dbiest.dirPrefs[re],'f',prec));
   ui->hl_2->setText(QString::number(dbiest.dirPrefs[hl],'f',prec));
   ui->hi_2->setText(QString::number(dbiest.dirPrefs[hi],'f',prec));
   ui->hr_2->setText(QString::number(dbiest.dirPrefs[hr],'f',prec));
   //-----------------
   ui->alter->setText(QString::number(dbiest.alter));
   ui->generation->setText(QString::number(dbiest.generation));

}

void Steuerwidget::enableSynchronizeCheckbox(bool enable)
{
  if(enable)
      ui->synchronize->show();
  else
      ui->synchronize->hide();
}

void Steuerwidget::checkSynchronizeCheckbox(bool checked)
{
    ui->synchronize->setChecked(checked);
}

void Steuerwidget::setDelay(int msec)
{
  ui->delay->setValue(msec);
}
