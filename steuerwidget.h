#ifndef STEUERWIDGET_H
#define STEUERWIDGET_H

#include <QDockWidget>
class Biotop;
namespace Ui {
class Steuerwidget;
}

class Steuerwidget : public QDockWidget
{
    Q_OBJECT

public:
    explicit Steuerwidget(QWidget *parent = nullptr,Biotop*biot=nullptr);
    ~Steuerwidget();
    void actualize();
    void enableSynchronizeCheckbox(bool enable=true);
    void checkSynchronizeCheckbox(bool checked=true);
    void setDelay(int msec);
private:
    Ui::Steuerwidget *ui;
    Biotop*biotop;
};

#endif // STEUERWIDGET_H
