#ifndef BIEST_H
#define BIEST_H
#include <QtCore>
#include <QtGui>
#include "standardparameter.h"
#include "farbmischungswahl.h"
class Biotop;
//static  QColor BakterienColor=QColor("orange");
//static  QColor BgColor=QColor(12,0,25);
double zufall();
enum Direction {vo=0,vr,re,hr,hi,hl,li,vl};
class Biest{
 public:
  Biest(float initialdirPref=0.125);
  Biest(const QSize & size, int startenergie);///setzt das Biest mit zufälligen Koordinaten in size
  Biest(int agewicht, float * adirPrefs);
  //~Biest();
  static QPixmap*pix;//pixmap für die Biester zum schnellen Zeichnen.
  static  int VerloeschGewicht;//=-100;
  bool gehe(const QSize & size, QImage & nahrung, ushort nahrhaftigkeit);///liefert false, wenn gewicht==0.
  void mutiere();//ändert die dirPrefs zufällig ab.
  void biegAb(Direction richtung); //ändert die Richtung
  void design();//öffnet einen Dialog zum Ändern der Biest-Eigenschaften
  QPoint getOrt() const {return ort;};
  QColor getColor() const {return color;};
  void setColor(QColor col){color=col;};
  void draw(QPainter & paint);
  void erase(QPainter & paint);
  int x() const {return ort.x();}
  int y() const {return ort.y();}
  void setX(int x){ort.setX(x);}
  void setY(int y){ort.setY(y);}
  int alter=0, gewicht,generation=0;
  float dirPrefs[16];//speichert in den ersten 8 Werten die Wahrscheinlichkeiten für Diagonalbewegung.
  //float * dirPrefsHV;//...die nächsten 8 für die Wahrscheinlichkeiten für Horizontal/Vertikalbewegung
  Direction v;//Bewegungsrichtung relativ zur 12-Uhr-Richtung
  static short size;
  static QColor randomColor(){return QColor((int)(zufall()*256),(int)(zufall()*256),(int)(zufall()*256));}
  QGroupBox*getInfoBox(const QString &title=QString(),QWidget*parent=nullptr)const;//liefert eine InfoBox mit den Werten des Biestes.
   friend QDataStream& operator<<(QDataStream&ds,const Biest&biest);
   friend QDataStream& operator>>(QDataStream&ds,Biest&biest);
 private:

  QPoint ort;
  QColor color;
};

//--------------------------BiestPopulation--------------------------------

class BiestPopulation : public QList<Biest>{
 public:
  uint living;
  BiestPopulation():QList<Biest>(){farbGenerator.biestvolk=this;};
  BiestPopulation(Biotop*abio,uint count=100);
  //~BiestPopulation();
  //enum {dirPref, random} farbgebung=random;
  FarbGenerator farbGenerator;
  //std::function<QColor(float*)> getBiestColor;
  void removeDeads();
  void step(QImage&nahrung);
  int schnittGeneration();
  Biest  durchschnittsBiest(QRect rect=QRect(),uint*count=nullptr);///ermittelt den Durchschnitt in rect. Die Anzahl der Biester wird in count zurückgegeben.
  int biestAt(QPoint p);///liefert index des dem Punkt p nächstgelegene Biest, -1 wenns keine Biester gibt, -2 wenn weiter als 10 Pixel entfernt.
  Direction abbiegrichtung=vr;
  int ReproGewicht=Standard::ReproGewicht;//=100;
  int StartEnergie=Standard::StartEnergie;//=99;
  void chooseGeneticColor();
  void chooseRandomColor();
  void colorize(){for (Biest&b:*this){b.setColor(farbGenerator.getColor(b.dirPrefs));}};//färbt alle Biester mit dem farbGenerator
  Biotop*biotop;
  friend QDataStream&operator<<(QDataStream&ds,const BiestPopulation&pop);
  friend QDataStream&operator>>(QDataStream&ds,BiestPopulation&pop);

};
#endif
