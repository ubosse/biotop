#ifndef STANDARD_H
#define STANDARD_H
namespace Standard {
  const int Mannarate=70;//soviel Bakterien pro 1000.000 pixel pro Schritt.
  const int Nahrhaftigkeit=15;//soviel Schritte kann ein Biest mit einem Bakterium gehen.
  const int Startpopulation=Mannarate*Nahrhaftigkeit/3;
  const int VerloeschGewicht=-100;
  const int ReproGewicht=100;
  const int StartEnergie=99;
  const short BiestSize=5;
}
#endif
