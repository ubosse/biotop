#include "biotopmainwindow.h"
#include "ui_biotopmainwindow.h"
#include "steuerwidget.h"

BiotopMainWindow::BiotopMainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::BiotopMainWindow)
{
    if(synchronizer==nullptr){
        synchronizer=new BiotopSynchronizer;
    }
    setAttribute(Qt::WA_DeleteOnClose);
    ui->setupUi(this);
    resize(1203,812);
    biotop=new Biotop(this,QSize(1024,768));
    QAction*act=new QAction(this);
    act->setShortcut(QKeySequence(Qt::Key_P));
    connect(act,&QAction::triggered,[this](){biotop->startStop();});
    addAction(act);
    connect(ui->actionEnde,&QAction::triggered,qApp,&QApplication::quit);
    connect(ui->actionNeu,&QAction::triggered,biotop,&Biotop::restart);
    connect(ui->actionNewBiotop,&QAction::triggered,this,&BiotopMainWindow::newBiotop);
    connect(ui->actionParameter,&QAction::triggered,[this](){biotop->renew(false);});
    connect(ui->action_randomColor,&QAction::triggered,[this](){biotop->biestvolk->chooseRandomColor();});
    connect(ui->action_sichern,&QAction::triggered,this,&BiotopMainWindow::saveBiotop);
    connect(ui->action_laden,&QAction::triggered,this,&BiotopMainWindow::loadBiotop);
    //connect(ui->action_geneticColor,&QAction::triggered,[this](){biotop->biestvolk->chooseGeneticColor();});
    connect(ui->actionFarbmischdialog_anzeigen,&QAction::triggered,[this](){biotop->biestvolk->farbGenerator.openFarbmischdialog();});
    QAction * helpaction=menuBar()->addAction("Spielregeln");
    connect(helpaction,&QAction::triggered,this,&BiotopMainWindow::showHelp);
    setCentralWidget(biotop);
    steuerwidget=new Steuerwidget(this,biotop);
    addDockWidget(Qt::LeftDockWidgetArea,steuerwidget);
    QTimer * actualizeTimer=new QTimer(this);
    actualizeTimer->setInterval(333);
    actualizeTimer->callOnTimeout(steuerwidget,&Steuerwidget::actualize);
    actualizeTimer->start();
    setWindowTitle("Biester-Biotop mit Bakteriennahrung");
    synchronizer->addClient(this);
    biotop->show();
}

BiotopMainWindow::~BiotopMainWindow()
{
    delete ui;
    synchronizer->removeClient(this);
}

void BiotopMainWindow::newBiotop()
{
    BiotopMainWindow* neuesFenster=new BiotopMainWindow(0);
    neuesFenster->show();
}
void BiotopMainWindow::saveBiotop(){
    biotop->startStop(0);
    QString filename=QFileDialog::getSaveFileName(0,"Dateiname wählen",".","Biotopdatein (*.biotop)");
    if(filename.isEmpty())
        return;
    if(!filename.endsWith(".biotop")){
        filename+=".biotop";
    }
    QFile f(filename);
    if(!f.open(QIODevice::WriteOnly)){
        QMessageBox::warning(0,"Mist","Kann die Datei "+filename+" nicht zum Schreiben öffnen");
        return;
    }
    QDataStream ds(&f);
    ds<<size();
    ds<<*biotop;
    f.close();
    QMessageBox::information(0,"Info","Biotop wurde in "+filename+" gesichert");
}
void BiotopMainWindow::loadBiotop()
{
    biotop->startStop(0);
    QString filename=QFileDialog::getOpenFileName(0,"Biotopdatei wählen",".",
                                                  "Biotopdatei (*.biotop)");
    if(filename.isEmpty())
        return;
    QFile f(filename);
    if(!f.open(QIODevice::ReadOnly)){
        QMessageBox::warning(0,"Mist","Kann die Datei "+filename+" nicht zum Lesen öffnen");
        return;
    }
    QDataStream ds(&f);
    QSize siz;
    ds>>siz;
    resize(siz);
    ds>>*biotop;
    biotop->repaint();
}

void BiotopMainWindow::showHelp()
{
    QTextBrowser*tb=new QTextBrowser;
    tb->setSearchPaths(QStringList()<<"/hilfeseiten"<<"");
    tb->setSource(QUrl("qrc:///hilfeseiten/help.html"));
    tb->setAttribute(Qt::WA_DeleteOnClose);
    tb->setWindowTitle("Spielregeln und Bedienung der Evolutionsevolution");
    tb->resize(1024,800);
    tb->show();
}
BiotopSynchronizer* BiotopMainWindow::synchronizer=nullptr;
/**-------------BiotopSynchronizer-Methoden----------------------------**/
BiotopSynchronizer::BiotopSynchronizer()
{
  connect(&timer,&QTimer::timeout,this,&BiotopSynchronizer::tick);
  timer.setInterval(50);
}
void BiotopSynchronizer::start(){
    timer.start();
}
void BiotopSynchronizer::stop(){
    timer.stop();
}
void BiotopSynchronizer::startStop(ushort state)
{
  switch(state){
    case 0:stop();break;
    case 1:start(); break;
    default:if(timer.isActive())stop(); else start(); break;
  }
}

void BiotopSynchronizer::setDelay(int del)
{
   if(timer.interval()==del) return;
   timer.setInterval(del);
   for(BiotopMainWindow*w:clients){
       w->steuerwidget->setDelay(del);
   }

}

void BiotopSynchronizer::addClient(BiotopMainWindow *cl)
{
    clients<<cl;
    isActive=clients.count()>1;
    for(BiotopMainWindow*w:clients){
        w->steuerwidget->enableSynchronizeCheckbox(isActive);
    }
}

void BiotopSynchronizer::removeClient(BiotopMainWindow *cl)
{
    clients.removeOne(cl);
    setActive(clients.count()>1);
    for(BiotopMainWindow*w:clients){
        w->steuerwidget->enableSynchronizeCheckbox(isActive);
    }

}

void BiotopSynchronizer::setActive(int active)
{
  if(isActive==active)return;
  isActive=active;
  for (BiotopMainWindow*w:clients){
      w->steuerwidget->checkSynchronizeCheckbox(isActive);
      if(isActive) w->biotop->timer->stop();
  }
  if(!isActive)
      timer.stop();
}
void BiotopSynchronizer::tick(){
    for(BiotopMainWindow*w:clients){
        w->biotop->nextStep();
    }
}
