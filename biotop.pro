QT += core gui widgets
CONFIG += c++11
SOURCES	+= biotop.cpp \
           biest.cpp \
           farbmischungswahl.cpp \
           main.cpp  \
           biotopmainwindow.cpp \
           steuerwidget.cpp
HEADERS	+= biotop.h \
           biotopmainwindow.h \
           biest.h \
           farbmischungswahl.h \
           standardparameter.h \
           steuerwidget.h
FORMS	=  biotopmainwindow.ui \
    biesteditor.ui \
    parametersettings.ui \
    steuerwidget.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    hilfeseiten.qrc



