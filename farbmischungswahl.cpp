#include "farbmischungswahl.h"
#include "biotop.h"
#include <QtWidgets>
FarbMischungsWahl::FarbMischungsWahl(QString title, QList<FarbGenerator::funcInfo> *collist, BiestPopulation *volk, QWidget *parent)
    : QGroupBox{parent}
{
  setObjectName(title);
  biestvolk=volk;
  colorlist=collist;
  mspin=new QDoubleSpinBox(this);
  p0spin=new QDoubleSpinBox(this);
  mspin->blockSignals(true);
  p0spin->blockSignals(true);
  for(short i=0;i<16;i++){//alle Checkboxen erzeugen.
      DChecks[i]=new LinearBox(this);
  }
  for(auto fI:*colorlist){//Checkboxen initialisieren.
      DChecks[fI.dir]->p0=fI.p0;//für diese Farbe m und p0 setzen...
      DChecks[fI.dir]->m=fI.m;
      DChecks[fI.dir]->setChecked(true);//...bei den gecheckten Richtungen.
      lastBox=fI.dir;
  }
  //jetzt die mspin p0spin setzen gemäß des letzen gecheckten Wertes.
  DChecks[lastBox]->setText("←↴");
  mspin->setValue(DChecks[lastBox]->m);
  p0spin->setValue(DChecks[lastBox]->p0);
  for(short i=0;i<16;i++){//stateChanged Signal der Checkboxen verbinden.
      connect(DChecks[i],&QCheckBox::stateChanged,[this,i](int state){
          //qDebug()<<i<<"DCheck stateChanged";
          if(state==Qt::Checked){
              mspin->blockSignals(true);p0spin->blockSignals(true);
              mspin->setValue(DChecks[i]->m);p0spin->setValue(DChecks[i]->p0);//Wert aus Puffer in editfelder
              mspin->blockSignals(false);p0spin->blockSignals(false);
              colorlist->append(FarbGenerator::funcInfo(i,DChecks[i]->m,DChecks[i]->p0));
              DChecks[lastBox]->setText("");
              lastBox=i;
              DChecks[lastBox]->setText("←↴");
          }else{
              colorlist->removeIf([i](FarbGenerator::funcInfo fI){return fI.dir==i;});
          }
          biestvolk->colorize();
      });
  }
  //jetzt die valueChanged-Signal des mspin verbinden
  connect(mspin,&QDoubleSpinBox::valueChanged,[this](double value){
      DChecks[lastBox]->m=value;
      colorlist->removeIf([this](FarbGenerator::funcInfo fI){return fI.dir==lastBox;});
      colorlist->append(FarbGenerator::funcInfo(lastBox,value,DChecks[lastBox]->p0));
      biestvolk->colorize();
  });
  connect(p0spin,&QDoubleSpinBox::valueChanged,[this](double value){//das gleiche mit p0spin
      DChecks[lastBox]->p0=value;
      colorlist->removeIf([this](FarbGenerator::funcInfo fI){return fI.dir==lastBox;});
      colorlist->append(FarbGenerator::funcInfo(lastBox,DChecks[lastBox]->m,value));
      biestvolk->colorize();
  });
  QGridLayout*lay=new QGridLayout;
  lay->setObjectName("GridLayout");
  setLayout(lay);
  setTitle(title);setAlignment(Qt::AlignCenter);
  setFlat(false);
  //lay->addWidget(new QLabel(title),0,0,1,3,Qt::AlignCenter);
  auto addWidgets=[lay](LinearBox*checks[],int from){
      lay->addWidget(checks[7],from,0,Qt::AlignCenter);
      lay->addWidget(checks[0],from,1,Qt::AlignCenter);
      lay->addWidget(checks[1],from,2,Qt::AlignCenter);
      lay->addWidget(checks[6],from+1,0,Qt::AlignCenter);
      lay->addWidget(checks[2],from+1,2,Qt::AlignCenter);
      lay->addWidget(checks[5],from+2,0,Qt::AlignCenter);
      lay->addWidget(checks[4],from+2,1,Qt::AlignCenter);
      lay->addWidget(checks[3],from+2,2,Qt::AlignCenter);
  };
  addWidgets(DChecks,0);
  lay->addWidget(new QLabel("↖ ↗"),1,1,Qt::AlignCenter);
  //---------------------Line-------------------------------
  QFrame*line=new QFrame(this);
  line->setLineWidth(1);
  line->setFrameShape(QFrame::HLine);
  lay->addWidget(line,3,0,1,3,Qt::AlignVCenter);
  //------------------HVChecks-----------------------------
  addWidgets(HVChecks,4);
  lay->addWidget(new QLabel("↕ ↔"),5,1,Qt::AlignCenter);
  //----------------Line-----------------------------------
  line=new QFrame(this);
  line->setLineWidth(1);
  line->setFrameShape(QFrame::HLine);
  lay->addWidget(line,8,0,1,3,Qt::AlignVCenter);
  //----------------Eingabefeld für m und p0-----------------
  lay->addWidget(new QLabel("Farbwert = m*(W.keit -p0)"),9,0,1,3,Qt::AlignCenter);
  QHBoxLayout*hlay=new QHBoxLayout;
  hlay->addWidget(new QLabel("m="));
  mspin->setDecimals(2);p0spin->setDecimals(2);
  //mspin->setValue(1);
  mspin->setSingleStep(0.05);p0spin->setSingleStep(0.05);
  mspin->setMinimum(-2);p0spin->setMinimum(0);
  mspin->setMaximum(4);p0spin->setMaximum(2);
  hlay->addWidget(mspin);
  hlay->addWidget(new QLabel(" p0="),1,Qt::AlignRight);
  hlay->addWidget(p0spin);
  lay->addLayout(hlay,10,0,1,3);
  mspin->blockSignals(false);
  p0spin->blockSignals(false);
}

FarbGenerator::~FarbGenerator()
{
  if(farbmischdialog) farbmischdialog->close();
}

QColor FarbGenerator::getColor(float *dirPrefs)
{
    float rgb[3]={0,0,0};
    if(simple){
        for(const funcInfo & info:redlist)
            rgb[0]+=dirPrefs[info.dir];
        for(const funcInfo & info:greenlist)
            rgb[1]+=dirPrefs[info.dir];
        for(const funcInfo &info:bluelist)
            rgb[2]+=dirPrefs[info.dir];
    }else{
        for(const funcInfo & info:redlist)
            rgb[0]+=info.m*(dirPrefs[info.dir]-info.p0);
        for(const funcInfo & info:greenlist)
            rgb[1]+=info.m*(dirPrefs[info.dir]-info.p0);
        for(const funcInfo &info:bluelist)
            rgb[2]+=info.m*(dirPrefs[info.dir]-info.p0);
    }
    for(float &f :rgb){
      if(f>=1)
          f=1.0*255/256;
      if(f<0) f=0.0;
    }
    return QColor(rgb[0]*256,rgb[1]*256,rgb[2]*256);
}

void FarbGenerator::openFarbmischdialog()
{
    if(farbmischdialog!=nullptr){
        //QMessageBox::information(0,"Hinweis","Der Farbmischdialog sollte schon offen sein");
        farbmischdialog->show();farbmischdialog->raise();
        return;
    }
    farbmischdialog=new QDialog;
    QObject::connect(farbmischdialog,&QObject::destroyed,[this](){farbmischdialog=nullptr;});
    farbmischdialog->setAttribute(Qt::WA_DeleteOnClose);
    FarbMischungsWahl *red, *green, *blue;
    red=new FarbMischungsWahl("Rotanteil aus...",&redlist,biestvolk,farbmischdialog);
    green=new FarbMischungsWahl("Grünanteil aus...",&greenlist,biestvolk,farbmischdialog);
    blue=new FarbMischungsWahl("Blauanteil aus...",&bluelist,biestvolk,farbmischdialog);
    QHBoxLayout * lay=new QHBoxLayout;//Layout für die FarbMischungswahlen
    QVBoxLayout*masterlay=new QVBoxLayout(farbmischdialog);//Layout des Dialoges
    farbmischdialog->setLayout(masterlay);
    lay->addWidget(red);
    lay->addWidget(green);
    lay->addWidget(blue);
    masterlay->addLayout(lay);
    QDialogButtonBox* bb=new QDialogButtonBox(QDialogButtonBox::Ok
                                              | QDialogButtonBox::Cancel|QDialogButtonBox::Help);
     QObject::connect(bb, &QDialogButtonBox::accepted, farbmischdialog, &QDialog::accept);
     QObject::connect(bb, &QDialogButtonBox::rejected, farbmischdialog, &QDialog::reject);
     QObject::connect(bb,&QDialogButtonBox::helpRequested,[](){
         QMessageBox::information(0,"Info","Die Farbanteile (rot,grün,blau) der Biestfarbe berechnen sich "
                                  "als Summe der hier im jeweiligen Feld angekreuzten Wahrscheinlichkeiten. "
                                  "Das Biest ist also umso röter, je höher die Summe der bei Rot angekreuzten "
                                  "Felder ist.\n"
                                  "Für die Feinabstimmung, ist es möglich zu jedem Feld eine lineare Funktion anzugeben, "
                                  "in die die Wahrscheinlichkeit eingesetzt wird bevor sie in die Summe einfließt.");
     });
     masterlay->addWidget(bb);
    farbmischdialog->show();
    method=dirPref;
}
QDataStream&operator<<(QDataStream&ds,const FarbGenerator::funcInfo&fI){
    return ds<<fI.dir<<fI.m<<fI.p0;
}
QDataStream&operator>>(QDataStream&ds,FarbGenerator::funcInfo&fI){
    return ds>>fI.dir>>fI.m>>fI.p0;
}
QDataStream&operator<<(QDataStream&ds,const FarbGenerator&fg){
    ds<<fg.redlist<<fg.greenlist<<fg.bluelist;
    ds<<fg.method<<fg.simple;
    return ds;
}
QDataStream&operator>>(QDataStream&ds,FarbGenerator&fg){
    ds>>fg.redlist>>fg.greenlist>>fg.bluelist;
    ds>>fg.method>>fg.simple;
    return ds;
}
