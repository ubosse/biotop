#include <QtGui>
#include <QtCore>
#include <QtWidgets>
#include "biest.h"
#include "biotop.h"
#include "standardparameter.h"
#include "farbmischungswahl.h"
double zufall(){
 return QRandomGenerator::global()->generateDouble();
}
/*------------------Implementation für die Biest-Methoden------------------------*/
//QPixmap* Biest::pix=new QPixmap(biestxpm);
short Biest::size=Standard::BiestSize;
int Biest::VerloeschGewicht=Standard::VerloeschGewicht;
Biest::Biest(float initialdirPref){//Standardkonstruktor, nullt alles aus
 generation=alter=gewicht=0;
 ort=QPoint(0,0);
 v=li;
 //dirPrefsHV=dirPrefsD+8;
 for(uint i=0;i<16;dirPrefs[i++]=initialdirPref);
 color=QColor((int)(zufall()*255),(int)(zufall()*255),(int)(zufall()*255));
}
Biest::Biest(const QSize & size,int startenergie){//setzt das Biest mit zufälligen Koordinaten in size, zufälliger farbe und dirPrefs.
  alter=0;
  generation=0;
  gewicht=startenergie;
  //dirPrefsHV=dirPrefsD+8;
  v=(Direction)(int)(zufall() * 8);
  ort=QPoint(size.width()*zufall(),
	     size.height()*zufall());
  float sum=0;
  for (int i=0;i<16;i++){
    float r=zufall();
    dirPrefs[i]=r;
  }
  for(int i=0;i<8;i++) sum+=dirPrefs[i];
  for (int i=0;i<8;i++) dirPrefs[i] /= sum;
  sum=0.0;
  for (int i=8;i<16;i++)    sum+=dirPrefs[i];
  for (int i=8;i<16;i++)    dirPrefs[i] /= sum;
  //jetzt sollte die Summe der dirPrefs 1 sein.
  color=randomColor();
}
bool Biest::gehe(const QSize & feld, QImage &nahrung,ushort nahrhaftigkeit){//lässt das Biest gemäß seiner dirPrefs einen Schritt gehen
  if(gewicht-- <= 0){
      if(gewicht==-1)
          return false;
      else
          return true;
  }
  alter++;
  uint i;
  float sum=0,r=zufall();//zufall ist zahl zwischen 0 und 1
  int ofset=(v%2==0)?8:0;//v%2==0 bei horizontaler Bewegung
  for (i=0;i<8;i++){
    sum+=dirPrefs[i+ofset];
    if(sum>=r)
      break;
  }// in Richtung i muss jetzt das Biest abbiegen.
  i=i>7?7:i;//eigentlich überflüssige Vorsichtsmaßnahme.
  v=(Direction)(((int)(v+i)) % 8);//v verändern gemäß dirPrefs
  switch (v){//hier wird das Biest in richtung v um einen Schritt versetzt.
  case vo:ort.setY(ort.y()==0?feld.height()-1:ort.y()-1);
    break;
  case vr:ort+=QPoint(1,-1);
    if(ort.x()>=feld.width())
      ort.setX(0);
    if(ort.y()<0)
      ort.setY(feld.height()-1);
    break;
  case re:ort.setX(ort.x()>=feld.width()?0:ort.x()+1);
    break;
  case hr:ort+=QPoint(1,1);
    if(ort.x()>=feld.width())
      ort.setX(0);
    if(ort.y()>=feld.height())
      ort.setY(0);
    break;
  case hi:ort.ry()++;
    if(ort.y()>=feld.height())
      ort.setY(0);
    break;
  case hl:ort+=QPoint(-1,1);
    if(ort.y()>=feld.height())
      ort.setY(0);
    if(ort.x()<0)
      ort.setX(feld.width()-1);
    break;
  case li:ort.rx()--;
    if(ort.x()<0)
      ort.setX(feld.width()-1);
    break;
  case vl:ort+=QPoint(-1,-1);
    if(ort.x()<0)
      ort.setX(feld.width()-1);
    if(ort.y()<0)
      ort.setY(feld.height()-1);
    break;
  }
  //jetzt die Nahrung aufnehmen -> pixel löschen im Image.
  unsigned short int ymax=ort.y()<=feld.height()-size?
    ort.y()+size:feld.height();
  unsigned short int xmax=ort.x()<=feld.width()-size?
    ort.x()+size:feld.width();
  //const uchar* daten=nahrung.bits();
  for(unsigned short int y=ort.y();y<ymax;y++){//schauen, ob Nahrung unter dem Biest ist.
    for(unsigned short int x=ort.x();x<xmax;x++){
      if(nahrung.pixelIndex(x,y)==1){
         nahrung.setPixel(x,y,0);
         gewicht+=nahrhaftigkeit;
      }
    }
  }
  return true;
}
void Biest::mutiere(){
  uint rand=(uint)(zufall()*256);
  uint pm1=(rand>>6)& 1;//Bit 6 wird herausgepickt
  uint pm2=rand>>7;     //Bit 7.
  uint d1=rand & 0b111;//HV-Richtung: Bit 0-2
  uint d2=(rand>>3) & 0b111;//D-Richtung: Bit 3-5
  //wenn >63 erhöhen, wenn <63 vermindern
  //%8 gibt d1 und /16 gibt d2 an.
  if(pm1==0)
    dirPrefs[d1+8]*=1.1;
  else
    dirPrefs[d1+8]/=1.1;
  float sum=0;
  uint d;
  for (d=0;d<8;sum+=dirPrefs[8+d++]);
  for (d=0;d<8;dirPrefs[8+d++]/=sum);
  if(pm2==0)
      dirPrefs[d2]*=1.1;
    else
      dirPrefs[d2]/=1.1;
    sum=0;
    for (d=0;d<8;sum+=dirPrefs[d++]);
    for (d=0;d<8;dirPrefs[d++]/=sum);
}
void Biest::biegAb(Direction richtung){//Methode wird bei der Teilung gebraucht.
    v=(Direction)(((uint)v+richtung) % 8);
}

QGroupBox *Biest::getInfoBox(const QString&title,QWidget *parent) const
{
    QGroupBox*gb=new QGroupBox(title,parent);
    QGridLayout*lay=new QGridLayout();
    gb->setLayout(lay);
    QLabel*dPl[16];
    const int prec=3;
    for(short i=0;i<16;i++){
        dPl[i]=new QLabel(QString::number(dirPrefs[i],'f',prec),gb);
        dPl[i]->setFrameStyle(QFrame::Box);
    }
    auto addRow=[lay](int row,QLabel*eins,QLabel*zwei,QLabel*drei){
        lay->addWidget(eins,row,0,Qt::AlignCenter);
        lay->addWidget(zwei,row,1,Qt::AlignCenter);
        lay->addWidget(drei,row,2,Qt::AlignCenter);
    };
    addRow(0,dPl[7],dPl[0],dPl[1]);
    addRow(1,dPl[6],new QLabel("↖  ↗"),dPl[2]);
    addRow(2,dPl[5],dPl[4],dPl[3]);
    QFrame*line=new QFrame(gb);
    line->setLineWidth(1);
    line->setFrameShape(QFrame::HLine);
    lay->addWidget(line,3,0,1,3,Qt::AlignVCenter);
    addRow(4,dPl[7+8],dPl[8],dPl[1+8]);
    addRow(5,dPl[6+8],new QLabel("↔ ↕"),dPl[2+8]);
    addRow(6,dPl[5+8],dPl[4+8],dPl[3+8]);
    line=new QFrame(gb);
    line->setLineWidth(1);
    line->setFrameShape(QFrame::HLine);
    lay->addWidget(line,7,0,1,3,Qt::AlignVCenter);
    QFormLayout* formlay=new QFormLayout;
    formlay->addRow("Alter:",new QLabel(QString::number(alter)));
    formlay->addRow("Generation:",new QLabel(QString::number(generation)));
    lay->addLayout(formlay,8,0,3,2,Qt::AlignCenter);
    return gb;
}
QDataStream& operator<<(QDataStream&ds,const Biest&b){
    ds<<b.alter;
    for(int i=0;i<16;i++) ds<<b.dirPrefs[i];
    ds<<b.generation;
    ds<<b.gewicht;
    ds<<b.v;
    ds<<b.color;
    ds<<b.ort;
    return ds;
}
QDataStream&operator>>(QDataStream&ds,Biest&b){
    ds>>b.alter;
    for(int i=0;i<16;i++) ds>>b.dirPrefs[i];
    ds>>b.generation;
    ds>>b.gewicht;
    ds>>b.v;
    ds>>b.color;
    ds>>b.ort;
    return ds;
}
/*------------Implementation der BiestPopulation-----------------*/
//uint BiestPopulation::living=0;
BiestPopulation::BiestPopulation(Biotop*biot,uint count):QList<Biest>(){
  for (uint i=0;i<count;i++){
    append(Biest(biot->size(),StartEnergie));
  }
  farbGenerator.biestvolk=this;
  living=count;
  biotop=biot;
}
void BiestPopulation::removeDeads(){
  /*auto it=constBegin();
  while(it!=end()){
      if((it->gewicht)<Biest::VerloeschGewicht){
          it=erase(it);
      }else{
          it++;
      }
  }
*/
    removeIf([](const Biest &b){return b.gewicht<Biest::VerloeschGewicht;});
}

void BiestPopulation::step(QImage&nahrung){
  BiestPopulation geburten;
  for(Biest&biest:*this){
      if(biest.gewicht>=ReproGewicht){
          biest.generation++;
          biest.alter=0;
          biest.gewicht /= 2;
          Biest biest2=biest;
          biest.mutiere();
          biest2.mutiere();
          if(farbGenerator.method==FarbGenerator::dirPref){
              biest.setColor(farbGenerator.getColor(biest.dirPrefs));
              biest2.setColor(farbGenerator.getColor(biest2.dirPrefs));
          }
          biest.biegAb(abbiegrichtung);
          geburten.append(biest2);
      }
      if(!biest.gehe(biotop->size(),nahrung,biotop->Nahrhaftigkeit))
          living-=1;
  }
  removeDeads();
  append(geburten);
  living+=geburten.count();
}

int BiestPopulation::schnittGeneration(){
  int sum=0;
  for(const Biest&biest:*this){
    sum+=biest.generation;
  }
  int c;
  return (c=count())>0?sum/c:-1;
}
Biest BiestPopulation::durchschnittsBiest(QRect rect,uint*count){
  if (isEmpty())
    return Biest();
  Biest dbiest(0.0);
  int c=0;
  for(const Biest&biest:*this){
      if(!rect.isNull()&&!rect.contains(biest.getOrt()))
          continue;
      dbiest.alter+=biest.alter;
      dbiest.generation+=biest.generation;
      dbiest.gewicht+=biest.gewicht;
      c++;
      for(uint i=0;i<16;i++)
        dbiest.dirPrefs[i]+=biest.dirPrefs[i];
  }
  if(count)*count=c;
  if(c==0)return dbiest;
  dbiest.alter/=c;
  dbiest.generation/=c;
  dbiest.gewicht/=c;
  for(uint i=0;i<16;dbiest.dirPrefs[i++]/=c);
  return dbiest;
}
int BiestPopulation::biestAt(QPoint p){
  if (isEmpty())
    return -1;
  int opt=-1;
  int dopt=2000000;//quadrat der geringsten euklidischen Entfernung
  for(int i=0;i<count();i++){
    const Biest&b=at(i);
    int d2=(p.x()-b.x())*(p.x()-b.x())+(p.y()-b.y())*(p.y()-b.y());
    if(d2<dopt){
      opt=i;
      dopt=d2;
    }
  }
#ifdef DEBUG
  qWarning("Biest bei (%d,%d) gefunden",opt->x(),opt->y());
#endif
  if (dopt<=100)
    return opt;
  else
      return -2;
}


void BiestPopulation::chooseRandomColor()
{
   farbGenerator.method=FarbGenerator::random;
   for(Biest&biest:*this){
       biest.setColor(Biest::randomColor());
   }
   biotop->repaint();
}
QDataStream&operator<<(QDataStream&ds,const BiestPopulation&pop){
    ds<<(QList<Biest>)pop;
    ds<<pop.abbiegrichtung;
    ds<<pop.farbGenerator;
    ds<<pop.ReproGewicht;
    ds<<pop.StartEnergie;
    return ds;
}
QDataStream&operator>>(QDataStream&ds,BiestPopulation&pop){
    int living=0;
    ds>>(QList<Biest>&)pop;
    for(Biest&b:pop) if(b.gewicht>0) living+=1;
    ds>>pop.abbiegrichtung;
    ds>>pop.farbGenerator;
    pop.farbGenerator.biestvolk=&pop;
    pop.farbGenerator.farbmischdialog=nullptr;
    ds>>pop.ReproGewicht;
    ds>>pop.StartEnergie;
    pop.living=living;
    return ds;
}
