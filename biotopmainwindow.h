#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "biotop.h"
namespace Ui {
class BiotopMainWindow;
}
class BiotopSynchronizer;
class Steuerwidget;
class BiotopMainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit BiotopMainWindow(QWidget *parent = nullptr);
    ~BiotopMainWindow();
    static BiotopSynchronizer*synchronizer;
    Steuerwidget*steuerwidget=nullptr;
private:
    Ui::BiotopMainWindow *ui;
    Biotop*biotop;
    void newBiotop();
    void saveBiotop();
    void loadBiotop();
    void showHelp();
    friend class BiotopSynchronizer;
};

class BiotopSynchronizer: public QObject
{
public:
    BiotopSynchronizer();
    void stop();
    void start();
    void startStop(ushort state);//0=stop, 1=start, 2=toggle
    void setDelay(int del);
    void addClient(BiotopMainWindow*cl);
    void removeClient(BiotopMainWindow*cl);
    bool isActive=false;
//public slots:
    void setActive(int active);
private:
    QList<BiotopMainWindow*> clients;
    QTimer timer;
    void tick();
};

#endif // MAINWINDOW_H
