//#define DEBUG
//#include <stdlib.h>
#include "biotop.h"
#include "standardparameter.h"
#include "ui_parametersettings.h"
#include "ui_biesteditor.h"
#include "biotopmainwindow.h"
Biotop::Biotop( QWidget* parent, QSize siz )
  : QWidget( parent)
{
    setObjectName( "Biotop" );
    resize(siz);
/**  Population und Bakterien erzeugen *****************************************************/
    biestvolk=new BiestPopulation(this,Startpopulation);
    //qDebug()<<"Ich habe jetzt "<<biestvolk->count()<<" Biester erzeugt.";
    bakterien=new QImage(size(),QImage::Format_Mono);
    bakterien->setColor(0,0xff000000);
    bakterien->setColor(1,0xffffffff);
    bakterien->fill(0);
    //const uchar*daten=bakterien->bits();
    //qDebug()<<daten;
    //qDebug()<<bakterien->colorTable();
    regnetManna(35*Nahrhaftigkeit*Mannarate);
//Timer-Einrichtung
    //uchar* start=bakterien->bits();
    delay=50;
    timer=new QTimer(this);
    timer->setInterval(delay);
    connect(timer,SIGNAL(timeout()),this,SLOT(nextStep()));
    sizeLabel = new QLabel(this);
    sizeLabel->hide();
    //Oasen= QList<Oase>();
 }
Biotop::~Biotop()
{
  if(biestvolk)  
    delete biestvolk;
  if(bakterien)
    delete bakterien;
}
void Biotop::restart(){
  if(biestvolk) delete biestvolk;
  changeMannarate(Standard::Mannarate);
  emit mannarateChanged(Mannarate);
  biestvolk=new BiestPopulation(this,(int)(Startpopulation));//1UL*Mannarate*Nahrhaftigkeit*width()*height()/3000000));
  lifetime=0;
  bakterien->fill(0);
  regnetManna(1UL*40*Nahrhaftigkeit*Mannarate*width()*height()/1000000);
  repaint();
  if(!BiotopMainWindow::synchronizer->isActive)
    timer->start(delay);

}

void Biotop::renew(bool andRestart)
{
  //biestvolk->clear();
  Ui::ParameterSettings ui;
  QDialog dial;
  ui.setupUi(&dial);
  ui.nahrhaftigkeit->setText(QString::number(Nahrhaftigkeit));
  ui.reprogewicht->setText(QString::number(biestvolk->ReproGewicht));
  ui.startgewicht->setText(QString::number(biestvolk->StartEnergie));
  ui.startpopulation->setText(QString::number(Startpopulation));
  ui.abbiegen->setValue(biestvolk->abbiegrichtung);
  ui.biestSize->setValue(Biest::size);
  connect(ui.standardValuesButton,&QPushButton::clicked,[ui](){
      ui.nahrhaftigkeit->setText(QString::number(Standard::Nahrhaftigkeit));
      ui.reprogewicht->setText(QString::number(Standard::ReproGewicht));
      ui.startgewicht->setText(QString::number(Standard::StartEnergie));
      ui.startpopulation->setText(QString::number(Standard::Startpopulation));
      ui.biestSize->setValue(Standard::BiestSize);
      ui.abbiegen->setValue(1);
  });
  dial.show();
  if(dial.exec()==QDialog::Accepted){
      bool ok;
      int wert=0;
      wert=ui.nahrhaftigkeit->text().toInt(&ok);
      Nahrhaftigkeit=ok?wert:Standard::Nahrhaftigkeit;
      wert=ui.reprogewicht->text().toInt(&ok);
      biestvolk->ReproGewicht=ok?wert:Standard::ReproGewicht;
      wert=ui.startgewicht->text().toInt(&ok);
      biestvolk->StartEnergie=ok?wert:Standard::StartEnergie;
      wert=ui.startpopulation->text().toInt(&ok);
      Startpopulation=ok?wert:Standard::Startpopulation;
      biestvolk->abbiegrichtung=(Direction)ui.abbiegen->value();
      Biest::size=ui.biestSize->value();
  }
  if(andRestart) restart();
}
void Biotop::showPopulation(){
  repaint();
}
int Biotop::bakterienZahl(){
  int sum=0;
  for (int y=0;y<height();y++){
    for(int x=0;x<width();sum+=bakterien->pixelIndex(x++,y));
  }
  return sum;
}
void Biotop::nextStep(){//wird alle delay ms aufgerufen
  /*#ifdef DEBUG
  qWarning("nextStep() aufgerufen");
  #endif*/
  //showPopulation(); 
  //nextStepIsCalling=true;
  //biestvolk->removeDeads();
  /*  static bool speichern=true;
  if((biestvolk->living<=5)&&speichern){
      timer->stop();
      QFile file("biestpopulation");
      file.open(QIODevice::WriteOnly);
      QDataStream ds(&file);
      ds<<*biestvolk;
      file.close();
      file.open(QIODevice::ReadOnly);
      ds.setDevice(&file);
      BiestPopulation* pop=new BiestPopulation();
      ds>>*pop;
      pop->biotop=this;
      delete biestvolk;
      biestvolk=pop;
      speichern=false;

  }*/
  lifetime++;
  if (biestvolk->isEmpty()){
    timer->stop();
    QMessageBox  msg(QMessageBox::Information,"Trauerfall!!","Alle Biester sind ausgestorben! Was tun?");
    QAbstractButton*quitButton=msg.addButton("Programm Beenden",QMessageBox::AcceptRole);
    QAbstractButton*changeparButton=msg.addButton("Start mit neuen Parametern",QMessageBox::AcceptRole);
    msg.addButton("Start mit gleichen Parametern",QMessageBox::AcceptRole);
    msg.exec();
    QAbstractButton*result=msg.clickedButton();
    if(result==nullptr || result==quitButton){
       qApp->quit();
    }
    if(result==changeparButton){
        renew();
    }else

      restart();
  }
  regnetManna(absoluterate);
  for(const Oase&oase:oasen){
      int groesse=oase.bereich.width()*oase.bereich.height();
      for(int i=0;i<oase.getAbsoluterate();i++){
          uint zuf=QRandomGenerator::global()->bounded(groesse);
          const QRect &r=oase.bereich;
          bakterien->setPixel(zuf/r.height()+r.x(),zuf%r.height()+r.y(),1);
      }
  }
  biestvolk->step(*bakterien);/** und bewegdichfort **/
  repaint();
}
void Biotop::paintEvent(QPaintEvent*e){
  QPainter paint(this); 
  /*if(!(e->erased()))*/
     paint.drawImage(QPointF(0,0),*bakterien,bakterien->rect());//alle Bakterien zeichnen
     QRect hier(0,0,Biest::size,Biest::size);
     //QSize si=QSize(Biest::size,Biest::size);
     QBrush brush(Qt::SolidPattern);
     paint.setPen(Qt::red);
     for(const Biest &biest:*biestvolk){
         //hier=QRect(biest.getOrt(),si);
         hier.moveTo(biest.getOrt());
         brush.setColor(biest.getColor());
         //brush.setColor(Qt::blue);
         paint.setBrush(brush);
         paint.fillRect(hier,brush);
         //qDebug()<<"habe Biest gezeichnet";
     }
     //jetzt könnten die Oasen noch gezeichnet werden. Ist aber doof.
     if(selecting){
         QPen pen(Qt::white);
         pen.setWidth(1);
         paint.setPen(pen);
         paint.setBrush(Qt::NoBrush);
         paint.drawRect(selectedRect);
         /*
         sizeLabel->setText(QString("(%1,%2)").arg(selectedRect.width()).arg(selectedRect.height()));
         sizeLabel->move(selectedRect.topLeft());
         sizeLabel->resize(sizeLabel->sizeHint());
         sizeLabel->setStyleSheet("background-color:0xffffff;");
         sizeLabel->show();
         sizeLabel->raise();*/
         QToolTip::showText(mapToGlobal(selectedRect.topLeft()),QString("(%1,%2)").arg(selectedRect.width()).arg(selectedRect.height()));

     }

}

void Biotop::mousePressEvent(QMouseEvent *e)
{

    timer->stop();
    if(e->button()==Qt::RightButton){
        rechtsKlick(e);
        return;
    }
    bool neueBiester=false;
   if(e->modifiers()==Qt::NoModifier){//biest editieren bzw. erzeugen
       int b=biestvolk->biestAt(e->position().toPoint());
       Biest*biest=nullptr;
       if(b>=0&&b<biestvolk->count())
           biest=&((*biestvolk)[b]);
       else {
           biestvolk->append(Biest());
           b=biestvolk->count()-1;
           biest=&((*biestvolk)[b]);
           biest->setX(e->position().toPoint().x());
           biest->setY(e->position().toPoint().y());
           neueBiester=true;
       }
       Ui::BiestEditor ui;
       QDialog dial;
       ui.setupUi(&dial);
       ui.vr_2->setText(QString::number(biest->dirPrefs[vr],'f',3));
       ui.vo_2->setText(QString::number(biest->dirPrefs[vo],'f',3));
       ui.vl_2->setText(QString::number(biest->dirPrefs[vl],'f',3));
       ui.re_2->setText(QString::number(biest->dirPrefs[re],'f',3));
       ui.li_2->setText(QString::number(biest->dirPrefs[li],'f',3));
       ui.hr_2->setText(QString::number(biest->dirPrefs[hr],'f',3));
       ui.hi_2->setText(QString::number(biest->dirPrefs[hi],'f',3));
       ui.hl_2->setText(QString::number(biest->dirPrefs[hl],'f',3));
       ui.vr->setText(QString::number(biest->dirPrefs[vr+8],'f',3));
       ui.vo->setText(QString::number(biest->dirPrefs[vo+8],'f',3));
       ui.vl->setText(QString::number(biest->dirPrefs[vl+8],'f',3));
       ui.re->setText(QString::number(biest->dirPrefs[re+8],'f',3));
       ui.li->setText(QString::number(biest->dirPrefs[li+8],'f',3));
       ui.hr->setText(QString::number(biest->dirPrefs[hr+8],'f',3));
       ui.hi->setText(QString::number(biest->dirPrefs[hi+8],'f',3));
       ui.hl->setText(QString::number(biest->dirPrefs[hl+8],'f',3));
       ui.energy->setText(QString::number(neueBiester?99:biest->gewicht));
       ui.alter->setText(QString::number(biest->alter));
       ui.generation->setText(QString::number(biest->generation));
       ui.direction->setValue(biest->v);
       QColor c=biest->getColor();
       ui.farbButton->setStyleSheet("color:"+c.name(QColor::HexRgb));
       connect(ui.farbButton,&QPushButton::clicked,[ui,c,biest](){
           QColor nc=QColorDialog::getColor(c,0,"Biestfarbe wählen");
           if(nc.isValid()){
               biest->setColor(nc);
               ui.farbButton->setStyleSheet("color:"+nc.name(QColor::HexRgb));
           }
       });
       if(dial.exec()==QDialog::Accepted){
           bool ok;
           float f=ui.vl->text().toFloat(&ok);
           if(ok) biest->dirPrefs[vl+8]=f;
            f=ui.vo->text().toFloat(&ok);
           if(ok) biest->dirPrefs[vo+8]=f;
            f=ui.vr->text().toFloat(&ok);
           if(ok) biest->dirPrefs[vr+8]=f;
            f=ui.li->text().toFloat(&ok);
           if(ok) biest->dirPrefs[li+8]=f;
            f=ui.re->text().toFloat(&ok);
           if(ok) biest->dirPrefs[re+8]=f;
            f=ui.hl->text().toFloat(&ok);
           if(ok) biest->dirPrefs[hl+8]=f;
            f=ui.hi->text().toFloat(&ok);
           if(ok) biest->dirPrefs[hi+8]=f;
            f=ui.hr->text().toFloat(&ok);
           if(ok) biest->dirPrefs[hr+8]=f;
           float sum=0;
           for(int i=0;i<8;i++) sum+=biest->dirPrefs[i+8];
           for(int i=0;i<8;i++) biest->dirPrefs[i+8]/=sum;
           f=ui.vl_2->text().toFloat(&ok);
           if(ok) biest->dirPrefs[vl]=f;
            f=ui.vo_2->text().toFloat(&ok);
           if(ok) biest->dirPrefs[vo]=f;
            f=ui.vr_2->text().toFloat(&ok);
           if(ok) biest->dirPrefs[vr]=f;
            f=ui.li_2->text().toFloat(&ok);
           if(ok) biest->dirPrefs[li]=f;
            f=ui.re_2->text().toFloat(&ok);
           if(ok) biest->dirPrefs[re]=f;
            f=ui.hl_2->text().toFloat(&ok);
           if(ok) biest->dirPrefs[hl]=f;
            f=ui.hi->text().toFloat(&ok);
           if(ok) biest->dirPrefs[hi]=f;
            f=ui.hr->text().toFloat(&ok);
           if(ok) biest->dirPrefs[hr]=f;
           biest->v=(Direction)ui.direction->value();
           sum=0;
           for(int i=0;i<8;i++) sum+=biest->dirPrefs[i];
           for(int i=0;i<8;i++) biest->dirPrefs[i]/=sum;
           int en=ui.energy->text().toInt(&ok);
           if(ok) biest->gewicht=en;
       }
       repaint();

   }else if(e->modifiers()==Qt::ShiftModifier){//Bereich aufziehen.
       selecting=true;
       startSelect=e->position().toPoint();
       selectedRect=QRect(startSelect,startSelect);
   }
}
void Biotop::rechtsKlick(QMouseEvent*e){
    for(int i=0;i<oasen.count();i++){
        Oase & oas=oasen[i];
        if(oas.bereich.contains(e->position().toPoint()))
        {
            QMenu popup("was tun?");
            popup.addAction("Oase löschen",[this,i](){oasen.remove(i);});
            popup.addAction("Oase bearbeiten",[&oas](){
                int newRate=QInputDialog::getInt(0,"Oase ändern","neue Mannarate",oas.getRate(),0,300,10);
                oas.setRate(newRate);
            });
            popup.exec(mapToGlobal(e->position().toPoint()));
            break;
        }

    }
}
void Biotop::mouseMoveEvent(QMouseEvent *e)
{
    selectedRect=QRect(startSelect,e->position().toPoint()).normalized();
    selectedRect.setWidth(selectedRect.width()-selectedRect.width()%10);
    selectedRect.setHeight(selectedRect.height()-selectedRect.height()%10);
    repaint();
}
void Biotop::mouseReleaseEvent(QMouseEvent *e)
{
    if(selecting)//es wurde ein Rahmen aufgezogen
    {
        QMenu *popup=new QMenu("Aktionen mit Bereich");
        popup->addAction("Bereich zur Oase machen",[this](){
            Oase oase(selectedRect,-1);
            if(oase.getAbsoluterate()>0)
                oasen<<oase;
        });
        popup->addAction("Bereich biestfrei machen",[this](){
            for(Biest&b:*biestvolk){
                if(selectedRect.contains(b.getOrt()))
                    b.gewicht=b.VerloeschGewicht;
            }
        });
        popup->addAction("Bereich bakterienfrei machen",[this](){
            QPainter paint(bakterien);
            paint.fillRect(selectedRect,QColor(bakterien->colorTable().at(0)));
            paint.end();
        });
        popup->addAction("Durchschnittsbiest für diesen Bereich zeigen",[this](){
            uint count=0;
            Biest dbiest=biestvolk->durchschnittsBiest(selectedRect,&count);
            QGroupBox*dInfoBox=dbiest.getInfoBox("Durchschnitt über "+QString::number(count)+" Tiere");
            dInfoBox->setAttribute(Qt::WA_DeleteOnClose);
            dInfoBox->setWindowTitle("Bereichsinfo");
            dInfoBox->setWindowFlags(Qt::WindowStaysOnTopHint);
            dInfoBox->move(mapToGlobal(selectedRect.topLeft()));
            dInfoBox->show();
        });
        popup->exec(QCursor::pos());
        delete popup;
    }
    repaint();
    selecting=false;
}

void Biotop::resizeEvent(QResizeEvent * e){
  //QImage*neu=new QImage(bakterien->scaled(e->size(),Qt::IgnoreAspectRatio));

  QSize oldsize=bakterien->size();
  QRect oldrect=QRect(QPoint(0,0),oldsize);
  QRect newrect=QRect(QPoint(0,0),e->size()).intersected(oldrect);
  qDebug()<<"oldsize "<<e->oldSize()<<"new Size: "<<e->size()<<"image-size: "<<oldsize<<" intersect: "<<newrect;
  QImage * neu=new QImage(e->size(),QImage::Format_Mono);
  neu->setColorTable(bakterien->colorTable());
  neu->fill(0);
  QPainter paint(neu);
  paint.drawImage(newrect,*bakterien,newrect,Qt::MonoOnly);
  if(e->size().width()>bakterien->size().width())
  {
     paint.drawImage(QPoint(bakterien->size().width(),0),*bakterien);
  }
  if(e->size().height()>bakterien->size().height()){
     paint.drawImage(QPoint(0,bakterien->size().height()),*bakterien);
  }
  paint.end();
  delete bakterien;
  bakterien=neu;
  for(Biest&b:*biestvolk){
      QPoint old=b.getOrt();
      b.setX(1UL*old.x()*e->size().width()/oldsize.width());
      b.setY(1UL*old.y()*e->size().height()/oldsize.height());
      //qDebug()<<"Biest alt "<<old<<"neu "<<b.getOrt();
  }
  absoluterate=1UL*Mannarate*size().width()*size().height()/1000000;
  //parentWidget()->parentWidget()->setCaption(QString("Das Biester Biotop (%1,%2)").arg(size().width()).arg(size().height()));
}

void Biotop::changeDelay(int newDelay)
{
   delay=newDelay;timer->setInterval(newDelay);
   if(BiotopMainWindow::synchronizer->isActive){
       BiotopMainWindow::synchronizer->setDelay(newDelay);
   }
}

void Biotop::regnetManna(int soviel)
{
    if(soviel==-1) soviel=Mannarate;
   uchar*start= bakterien->bits();
   uint bound=8*bakterien->sizeInBytes();
   for(int i=0;i<soviel;i++){
       uint zuf=QRandomGenerator::global()->bounded(bound);//dieses Bit muss gesetzt werden.
       uchar&b=*(start+zuf/8);
       b |= 1<<(zuf%8);
   }
}



void Biotop::startStop(ushort state)
{
  if(!BiotopMainWindow::synchronizer->isActive){
      switch(state){
        case 0:timer->stop();break;
        case 1:timer->start();break;
        default:if(timer->isActive())
              timer->stop();
          else
              timer->start();
      }
  }else{
      BiotopMainWindow::synchronizer->startStop(state);
  }
}

void Biotop::forceParentResize(const QSize &newsize)
{
  QSize diff=parentWidget()->size();
  parentWidget()->resize(newsize+diff);
}

void Biotop::changeMannarate(int newMannarate){
      Mannarate=newMannarate;
      absoluterate=1UL*Mannarate*size().width()*size().height()/1000000;
      //qWarning("Mannarate auf %d gesetzt",Mannarate);
}


Oase::Oase(const QRect &hier, int rate)
{
    if(rate<0){
       rate=QInputDialog::getInt(0,"Mannarate für die Oase","zusätzliche Mannarate",70,0,300,5);
    }
    bereich=hier;
    setRate(rate);
}
void Oase::setRate(int relative){
   relativerate=relative;
   absoluterate=1UL*relative*bereich.width()*bereich.height()/1000000;
}

QDataStream& operator<<(QDataStream&ds,const Oase&oas){
    return ds<<oas.bereich<<oas.absoluterate<<oas.relativerate;
}
QDataStream& operator>>(QDataStream&ds,Oase&oas){
    return ds>>oas.bereich>>oas.absoluterate>>oas.relativerate;
}

QDataStream& operator<<(QDataStream&ds,const Biotop&biot){
    return ds<<*biot.bakterien<<*biot.biestvolk<<biot.delay<<biot.lifetime<<biot.Mannarate<<biot.Nahrhaftigkeit<<
               biot.abbiegrichtung<<biot.absoluterate<<biot.oasen;
}
QDataStream& operator>>(QDataStream&ds,Biotop&biot){
   //QImage bakts;
   ds>>*biot.bakterien;
   //*biot.bakterien=bakts;
   biot.biestvolk->clear();
   ds>>*biot.biestvolk;
   ds>>biot.delay>>biot.lifetime>>biot.Mannarate>>biot.Nahrhaftigkeit>>biot.abbiegrichtung>>biot.absoluterate>>biot.oasen;
   return ds;
}
