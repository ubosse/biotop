#ifndef FARBMISCHUNGSWAHL_H
#define FARBMISCHUNGSWAHL_H
#include <QGroupBox>
#include <QDialog>
#include <QCheckBox>

class QCheckBox;
class QDoubleSpinBox;
class Biotop;
class BiestPopulation;
class FarbMischDialog;
class FarbGenerator{//merkt sich, wie die Farbe der Biester entsteht und bietet Farbbestimmungsmethode
public:
    enum ColorMethod{dirPref=0,random};///dirPref: Farbbestimmung nach dirPref-Tabelle, random: zufällig
    struct funcInfo{
      ushort dir;float m;float p0;
      funcInfo():dir(0),m(1.0),p0(0.0){};
      funcInfo(ushort adir,float am,float ac):dir(adir),m(am),p0(ac){};
    };//m*(dirPrefs[dir]-p0)  geht in die Farbkomponente ein...
    ~FarbGenerator();
    bool simple=false; //...falls simple=false, sonst einfach nur dirPrefs[i];
    QList<funcInfo> redlist,greenlist,bluelist;//Rotwert=Summe_j (redlist[j].m*dirPrefs[redlist[j].dir]+redlist[j].c
    QDialog * farbmischdialog=nullptr;///nichtmodaler Dialog, der die Listen verändert.
    QColor getColor(float*dirPrefs);///greift auf die Listen redlist, greenlist, bluelist zu, um die Farbe aus dirPrefs zu bestimmen.
    //bool setGetColor();///setzt die Listen redlist, greenlist,bluelist. Sollte überflüssig werden.
    void openFarbmischdialog();///öffnet den Farbmischdialog nichtmodal. Der verändert die Listen.
    //void updateColorlists();///holt die Werte aus dem Farbmischdialog und updated die farblisten daraus.
    ColorMethod method=random;///gibt an, nach welcher Methode die Farbe der Biester gefunden wird.
    BiestPopulation * biestvolk=nullptr;
    friend QDataStream&operator<<(QDataStream&ds,const FarbGenerator&fg);
    friend QDataStream&operator>>(QDataStream&ds,FarbGenerator&fg);
};
class FarbMischungsWahl : public QGroupBox
{
public:
    class LinearBox : public QCheckBox{///Checkbox mit einem m und p0. Wenn gecheckt, dann sagen m und p: groß der Beitrag sein soll.
    public:
        LinearBox(QWidget*parent):QCheckBox(parent){}
        float m=1.0,p0=0.0;
        LinearBox(float am, float ap0, QWidget*parent):QCheckBox(parent),m(am),p0(ap0){};
    };
    explicit FarbMischungsWahl(QString title,QList<FarbGenerator::funcInfo>*collist, BiestPopulation *volk,QWidget *parent = nullptr);
    QList<FarbGenerator::funcInfo>*colorlist;///Für einen Farbanteil: Liste der Felder inkl. m und p, die zu diesem Farbanteil beitragen
    BiestPopulation * biestvolk;
    LinearBox* DChecks[16];
    LinearBox** HVChecks=DChecks+8;
    QDoubleSpinBox* mspin;
    QDoubleSpinBox* p0spin;
    short lastBox=0;///Werte 0..15:zuletzt gecheckte Box. Deren m und p0 werden angezeigt unten.

};


#endif // FARBMISCHUNGSWAHL_H
